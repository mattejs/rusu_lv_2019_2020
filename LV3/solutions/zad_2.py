import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

mtcars = pd.read_csv("mtcars.csv")

"""
#1 . Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara. 

cyl4 = mtcars[(mtcars.cyl == 4)]
cyl6 = mtcars[(mtcars.cyl == 6)]
cyl8 = mtcars[(mtcars.cyl == 8)]

plt.figure(figsize=[10,10])
plt.barh(cyl4.car, cyl4.mpg)
plt.barh(cyl6.car, cyl6.mpg)
plt.barh(cyl8.car, cyl8.mpg)
plt.legend(labels=['cyl4', 'cyl6', 'cyl8'])

"""
"""
# 2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.

cyl4 = mtcars[(mtcars.cyl == 4)]
cyl6 = mtcars[(mtcars.cyl == 6)]
cyl8 = mtcars[(mtcars.cyl == 8)]

plt.boxplot([cyl4.wt, cyl6.wt, cyl8.wt], positions=[4,6,8])
plt.xlabel("Broj cilindara")
plt.ylabel("Težine")
plt.grid()

"""
"""
# 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem 
#    veću potrošnju od automobila s automatskim mjenjačem? 

am0 = mtcars[(mtcars.am == 0)]
am1 = mtcars[(mtcars.am == 1)]
plt.boxplot([am0.mpg, am1.mpg], positions=[0,1])
plt.legend(["0 = Automatski","1 = Ručni"])

"""
"""
# 4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
#    mjenjačem.

am0 = mtcars[(mtcars.am == 0)]
am1 = mtcars[(mtcars.am == 1)]

plt.scatter(am0.qsec, am0.hp, marker='x')   
plt.scatter(am1.qsec, am1.hp, marker='o')  
plt.ylabel('Snaga')
plt.xlabel('Ubrzanje')
plt.legend(["Automatski","Ručni"])
plt.grid()

""" 
