import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

mtcars = pd.read_csv("mtcars.csv")

print("1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort) \n")
print(mtcars.sort_values('mpg').tail(5).car, "\n")

print("2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju? \n")
print(mtcars[(mtcars.cyl==8)].sort_values('mpg').head(3).car, "\n")

print("3. Kolika je srednja potrošnja automobila sa 6 cilindara? \n")
print(mtcars[(mtcars.cyl==6)].mean().mpg, "\n")

print("4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs? \n")
print(mtcars[(mtcars.cyl==4) & (mtcars.wt >= 2) & (mtcars.wt <= 2.2)].mean().mpg, "\n")

print("5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka? \n")
print("Ručni: ", mtcars[(mtcars.am==1)].count().am, "\nAutomatski: ", mtcars[(mtcars.am==0)].count().am, "\n")

print("6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga? \n")
print(mtcars[(mtcars.am==0) & (mtcars.hp > 100)].count().hp, "\n")

print("7. Kolika je masa svakog automobila u kilogramima? \n")
mtcars["wtkg"] = mtcars.wt*1000*0.45359237
print(mtcars[["car","wtkg"]])