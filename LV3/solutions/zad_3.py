import urllib.request
import matplotlib.pyplot as plt
import pandas as pd
import xml.etree.ElementTree as ET


url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = list(list(root)[i])
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1


df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
#df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

# 2. Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.
print(df.sort_values(by=['mjerenje']).tail(3).vrijeme)

# 3. Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.
months = [31,28,31,30,31,30,31,31,30,31,30,31] 
values = df.groupby('month').mjerenje.count()
missingValues = months - values
missingValues.plot.bar()
plt.ylabel('Izostale vrijednosti')
plt.xlabel('Mjeseci')

# 4. Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca. 
months = df[(df.month == 2) | (df.month == 8)]
months.boxplot(column = 'mjerenje', by ='month')
plt.ylabel('Koncentracija PM10 čestica')
plt.xlabel('Mjesec')

# 5. Usporedbu distribucije PM10čestica tijekom radnih dana s distribucijom čestica tijekom vikenda. 
df['weekend'] = df.dayOfweek > 4
df.boxplot(column = ['mjerenje'], by = 'weekend')
plt.ylabel('Koncentracija PM10 čestica')
plt.xlabel('')
plt.xticks([1, 2], ['Radni dan', 'Vikend'])










