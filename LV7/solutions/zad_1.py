import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
import sklearn.metrics as metrics
from sklearn import preprocessing

def generate_data(n):
	
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    
    return data

def plot_confusion_matrix(c_matrix, size):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

np.random.seed(242)
train_data = generate_data(200)
train_data = preprocessing.scale(train_data)
test_data = generate_data(100)
test_data = preprocessing.scale(test_data)

hidden_layers = [1, 2, 5, 10, 20, 50, 100]
alphas = [0.001, 0.01, 0.1, 1, 5, 10]

for i in range(len(hidden_layers)):
    for j in range(len(alphas)):
        clf = MLPClassifier(solver='lbfgs', alpha=alphas[j], hidden_layer_sizes=(hidden_layers[i]))        
        clf.fit(train_data[:,0:2],train_data[:,2])
        
        predicted = clf.predict(test_data[:,0:2])
        
        x_grid, y_grid = np.mgrid[min(train_data[:,0])-0.5:max(train_data[:,0])+0.5:.05,
                                  min(train_data[:,1])-0.5:max(train_data[:,1])+0.5:.05]
        grid = np.c_[x_grid.ravel(), y_grid.ravel()]
        probs = clf.predict_proba(grid)[:, 1].reshape(x_grid.shape)
        
        cont = plt.contour(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1, levels=[.5])
        plt.scatter(test_data[:,0],test_data[:,1],c=predicted)
        plt.title('%s neurons in hidden layer (alpha = %.3f)' %(hidden_layers[i], alphas[j]))
        
        plot_confusion_matrix(metrics.confusion_matrix(test_data[:,2], predicted), hidden_layers[i])
        
        print('\n%d neurons in hidden layer(alpha = %.3f):' %(hidden_layers[i], alphas[j]))
        accuracy = metrics.accuracy_score(test_data[:,2], predicted)
        missclassificationRate = 1-metrics.accuracy_score(test_data[:,2], predicted)
        precision = metrics.precision_score(test_data[:,2], predicted)
        recall = metrics.recall_score(test_data[:,2], predicted)
        tn, fp, fn, tp = metrics.confusion_matrix(test_data[:,2], predicted).ravel()
        specificity = tn / (tn+fp)
        
        print('Accuracy = ' + str(accuracy))
        print('Missclassification rate = ' + str(missclassificationRate))
        print('Precision = ' + str(precision))
        print('Recall = ' + str(recall))
        print('Specificity = ' + str(specificity))

