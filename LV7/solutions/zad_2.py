import numpy as np
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T

np.random.seed(242)

train_data = non_func(200)
test_data = non_func(100)

network = MLPRegressor(hidden_layer_sizes=(1000,500,500), alpha=0.00001, max_iter=1000)
network.fit(train_data[:,0].reshape(-1,1), train_data[:,2])

predicted_network = network.predict(test_data[:,0].reshape(-1,1))

poly = PolynomialFeatures(1, include_bias = False)
train_data_new = poly.fit_transform(train_data[:,0:2])
train_data_new = np.append(train_data_new, train_data[:,2:3], axis=1)
test_data_new = poly.fit_transform(test_data[:,0:2])
test_data_new = np.append(test_data_new, test_data[:,2:3], axis=1)

linReg = lm.LinearRegression()
linReg.fit(train_data_new[:, 0:-1], train_data_new[:, -1])

predicted_linReg = linReg.predict(test_data_new[:, 0:-1])

plt.scatter(test_data[:,0], test_data[:,2], c='red')
plt.plot(test_data[:,0], predicted_network, c='green', label = 'Neural network')
plt.plot(test_data[:,0], predicted_linReg, c='blue', label = 'Linear regression')
plt.plot(test_data[:,0], test_data[:,1], c='black', label = 'Real', )
plt.legend()
