import numpy as np
from sklearn.datasets import fetch_openml
import joblib
import pickle
from sklearn.neural_network import MLPClassifier
import sklearn.metrics as metrics


mnist = fetch_openml('mnist_784', version=1, cache=True)
X, y = mnist.data, mnist.target

#print('Got MNIST with %d training- and %d test samples' % (len(X), len(y_test)))
#print('Image size is:')

# rescale the data, train/test split
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]


# TODO: build youw own neural network using sckitlearn MPLClassifier 
mlp_mnist = MLPClassifier(solver='adam', alpha=0.1, hidden_layer_sizes=(50))
mlp_mnist.fit(X_train,y_train)

# TODO: evaluate trained NN
predicted = mlp_mnist.predict(X_test)

accuracy = metrics.accuracy_score(y_test, predicted)
missclassificationRate = 1-metrics.accuracy_score(y_test, predicted)
precision = metrics.precision_score(y_test, predicted, average='macro')
recall = metrics.recall_score(y_test, predicted, average='macro')

print('Accuracy = ' + str(accuracy))
print('Missclassification rate = ' + str(missclassificationRate))
print('Precision = ' + str(precision))
print('Recall = ' + str(recall))

# save NN to disk
filename = "NN_model.sav"
joblib.dump(mlp_mnist, filename)


