Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.
Promjene koje su napravljene:
1. raw_input promjenjen u input
2. fnamex prepravljeno u fname
3. Stavljene zagrade kod print funkcije ( print('File cannot be opened:', fname) , print(counts) )

LV1 : Upoznavanje s Git-om i programskim jezikom Python.
1. Zadatak : Skripta za izračun zarade
2. Zadatak : Ispis kategorije u kojoj se unešeni broj nalazi
3. Zadatak : Isto kao i 1. samo se izračun obavlja u zasebnoj funkciji
4. Zadatak : Unos brojeva dok korisnik ne upiše "Done"
5. Zadatak : Pretraga tekstualne datoteke i računanje srednje vrijednosti pouzdanosti spam filtera
6. Zadatak : Pretraga tekstualne datoteke i izdvajanje traženih vrijednosti u listu
