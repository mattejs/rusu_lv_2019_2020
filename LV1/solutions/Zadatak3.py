#Zadatak 3

radniSati = float(input("Unesite broj radnih sati: "))
knPoSatu = float(input ("Unesite placu po radnom satu: "))

def ukupnoKn(radni_sati, kuna_po_satu):
    return radni_sati * kuna_po_satu

Ukupno = ukupnoKn(radniSati, knPoSatu)

print("Radni sati: %.2f h" % (radniSati))
print("Kn/h: %.2f" % (knPoSatu))
print("Ukupno: %.2f kn" % (Ukupno))