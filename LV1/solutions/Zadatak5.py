#Zadatak 5
#Napišite  program  koji  od  korisnika  zahtijeva  unos  imena  tekstualne  datoteke.  
#Program  nakon  toga  treba tražiti  linije oblika: 
#    X-DSPAM-Confidence: <neki_broj> 
#koje  predstavljaju  pouzdanost  korištenog  spam  filtra.  
#Potrebno  je  izračunati  srednju  vrijednost  pouzdanosti.  
#Koristite datoteke mbox.txt i mbox-short.txt

fname = input('Enter the file name: ')  
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()
    
values = []

for line in fhand:
    if "X-DSPAM-Confidence:" in line:
        lineSplit = line.split(" ")
        values.append(float(lineSplit[1].strip()))

average = sum(values)/len(values)

print("File name: ", fname)
print("Average X-DSPAM-Confidence: ", average)


