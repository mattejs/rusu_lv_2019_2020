#Zadatak 6
# Napišite Python skriptu koja će učitati tekstualnu datoteku
# te iz redova koji počinju s „From“ izdvojiti mail adresu te ju
# spremiti u listu. 
# Nadalje potrebno je napraviti dictionary koji sadrži 
# hostname (dio emaila iza znaka @) te koliko puta se
# pojavljuje svaki hostname u učitanoj datoteci. Koristite datoteku mbox-short.txt. Na ekran ispišite samo nekoliko
# email adresa te nekoliko zapisa u dictionary-u. 

fname = input('Enter the file name: ')  
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()
    

emails = []
domains =[]
dictionary = {}

for line in fhand:
    if(line.startswith('From')):
        email = line.split(" ")[1].strip()
        emails.append(email)
        domain = email.split("@")[1]
        domains.append(domain)

for domain in domains:
    dictionary[domain] = dictionary.get(domain, 0) + 1

print("Emails: ", emails[:10])
print("Domains: ", dictionary)
