#Zadatak 1
#Napišite  python  skriptu  koja  od  korisnika  zahtijeva  unos  radnih  sati  te  koliko  je  plaćen  po  radnom  satu.  
#Nakon  toga izračunajte koliko je korisnik zaradio i ispišite na ekran.

radniSati = float(input("Unesite broj radnih sati: "))
knPoSatu = float(input ("Unesite placu po radnom satu: "))

ukupno = radniSati * knPoSatu

print("Radni sati: %.2f h" % (radniSati))
print("Kn/h: %.2f" % (knPoSatu))
print("Ukupno: %.2f kn" % (ukupno))
