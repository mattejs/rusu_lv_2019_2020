#Zadatak 4
#Napišite program koji od korisnika zahtijeva unos brojeva u beskonačnoj petlji sve dok korisnik ne upiše „Done“ (bez navodnika).
#Nakon toga potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju,  minimalnu i  maksimalnu vrijednost.  
#Osigurajte  program  od  krivog  unosa  (npr.  slovo  umjesto  brojke)  na  način  da  program  zanemari  taj  unos  
#i ispiše odgovarajuću poruku.

lista = []

while True:
    userInput=input("Unesite broj: ")
    if userInput == "Done":
        break
    try:
        lista.append(float(userInput))
    except:
        print("Nije unesen broj")

brojac = len(lista)
minBroj = min(lista)
maxBroj = max(lista)
srednjaVrijednost = sum(lista)/brojac

print("Ukupano unesenih brojeva: %.1d" % brojac )
print("Najmanji broj: %.2f" % minBroj)
print("Najveci broj: %.2f" % maxBroj)
print("Srednj broj: %.2f" % srednjaVrijednost)
    