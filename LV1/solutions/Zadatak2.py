#Zadatak 2
#Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja nekakvu ocjenu i nalazi se između 0.0 i 1.0. 
#Ispišite kojoj kategoriji pripada ocjena na temelju sljedećih uvjeta: 
#    >= 0.9 A 
#    >= 0.8 B 
#    >= 0.7 C 
#    >= 0.6 D 
#    < 0.6 F 
#Ako  korisnik  nije  utipkao  broj,  ispišite  na  ekran  poruku  o  grešci  (koristite tryiexceptnaredbe).  
#Također,  ako  je broj izvan intervala [0.0 i 1.0] potrebno je ispisati odgovarajuću poruku. 

while True:
    try:
        number = float(input("Unesite broj: "))
        if(number <= 1 and number >= 0):    
            break
        else:
           print("Broj mora biti u intervalu [0.0 i 1.0]")     
    except ValueError:
        print("Niste unijeli broj, pokusajte ponovo")

if number >= 0.9 :
    print("Uneseni broj %.2f pripada kategoriji A" % (number))
elif number >= 0.8 and number < 0.9:
    print("Uneseni broj %.2f pripada kategoriji B" % (number))
elif number >= 0.7 and number < 0.8:
    print("Uneseni broj %.2f pripada kategoriji C" % (number))
elif number >= 0.6 and number < 0.7:
    print("Uneseni broj %.2f pripada kategoriji D" % (number))
elif number < 0.6:
    print("Uneseni broj %.2f pripada kategoriji F" % (number))