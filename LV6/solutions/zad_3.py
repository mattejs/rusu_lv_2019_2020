import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

np.random.seed(242)

train_data = generate_data(200)
test_data = generate_data(100)


logReg = lm.LogisticRegression()
logReg.fit(train_data[:, :2], train_data[:, 2])

xx = np.array([train_data[:, 0].min(), train_data[:, 0].max()])

c = -logReg.intercept_[0]/logReg.coef_[0][1]
m = -logReg.coef_[0][0]/logReg.coef_[0][1] 

yy = m*xx + c

plt.scatter(train_data[:,0], train_data[:,1], c = train_data[:,2])
plt.plot(xx, yy)