import re

fname = "mbox-short.txt"
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()


for line in fhand:
    line = line.rstrip()
    mails = re.findall('[a-zA-Z0-9._]+@[a-zA-Z0-9._]+', line)
    
    for i in range(len(mails)):
        print(mails[i].split("@")[0])
    
