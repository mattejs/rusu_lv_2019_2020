import numpy as np
import matplotlib.pyplot as plt

dices  = np.random.randint(1,7, 100)

plt.hist(dices,bins=15)
plt.grid()
plt.ylabel("Koliko puta je broj dobiven")
plt.xlabel("Broj na kocki")
plt.show()
