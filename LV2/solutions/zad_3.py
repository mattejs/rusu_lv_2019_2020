import numpy as np
import matplotlib.pyplot as plt

gender = np.random.randint(2, size = 20)
height = [0] * 20
male = []
female = []

for i in range(0,20):
    if (gender[i] == 1):
        height[i] = np.random.normal(180,7)
        male.append(height[i])
    else:
        height[i] = np.random.normal(167,7)
        female.append(height[i])

x=[female,male]
y=[0,1]
colors={0:'red',1:'blue'}

for xe,ye in zip(x,y):
    plt.scatter(xe,[ye] * len(xe),c=colors[ye])
    
plt.scatter(x=np.average(male),color='yellow', y=1)
plt.scatter(x=np.average(female),color='green', y=0)
plt.xlabel('Height')
plt.ylabel('1=Male, 0=Female')
plt.legend(["Female","Male"])    
plt.show()