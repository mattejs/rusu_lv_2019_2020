import numpy as np
import matplotlib.pyplot as plt

averages = []
standardDeviations = []

for i in range(0,1000):
    dices  = np.random.randint(1,7, 30)      
    averages.append(np.mean(dices))
    standardDeviations.append(np.std(averages))    

print(np.mean(averages))
print(np.std(averages))

plt.hist(averages)
plt.grid()
plt.show()

"""
Što se pokus više puta izvodi srednja vrijednost i standardna devijacija biti
će bliži stvarnim vrijednostima.
"""