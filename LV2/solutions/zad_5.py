import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

cars=pd.read_csv("mtcars.csv")

plt.figure()
plt.scatter(cars.hp, cars.mpg, c=cars.wt)
plt.colorbar()
plt.ylabel("mpg")
plt.xlabel("Hp")
plt.grid()
plt.show()

print("Average consumption: %.2f" % np.average(cars.mpg))
print("Min consumption: %.2f" % min(cars.mpg))
print("Max consumption: %.2f" % max(cars.mpg))


