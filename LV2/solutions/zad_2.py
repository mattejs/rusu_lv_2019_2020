import re

fname = "mbox-short.txt"
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()

oneOrMoreA = []
oneA = []
zeroA = []
digits = []
lowerCase = []

for line in fhand:
    line = line.rstrip()
    mails = re.findall('[a-zA-Z0-9._]+@[a-zA-Z0-9._]+', line)
    for i in range(len(mails)):
        if mails[i].count('a')>=1:
            oneOrMoreA.append(mails[0].split('@')[0])
        if mails[i].count('a')==1:
            oneA.append(mails[0].split('@')[0])
        if mails[i].count('a')==0:
            zeroA.append(mails[0].split('@')[0])
        if re.findall('[0-9]+', mails[0]):
            digits.append(mails[0].split('@')[0])
        if mails[i].islower():
            lowerCase.append(mails[0].split('@')[0])

print('One or more a: ', oneOrMoreA)
print('One a: ', oneA)
print('Zero a: ', zeroA)
print('Digits: ', digits)
print('lowerCase: ', lowerCase)

