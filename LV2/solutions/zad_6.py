import matplotlib.pyplot as plt
import matplotlib.image as mpimg

image = mpimg.imread('tiger.png')
image *= 2
imgplot = plt.imshow(image) 
